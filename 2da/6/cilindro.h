#ifndef CILINDRO__H 
#define CILINDRO__H 1
#include <stdio.h>
#include <stdlib.h>
#define M_PI 3.1415
#endif

float cilindro_diametro(float radio, float altura);
float cilindro_circunferencia(float radio, float altura);
float cilindro_base(float radio, float altura);
float cilindro_volumen(float radio, float altura);
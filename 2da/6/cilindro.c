#include "cilindro.h"

float cilindro_diametro(float radio, float altura)
{
	return radio*2;
}
float cilindro_circunferencia(float radio, float altura)
{
	return M_PI*radio;
}
float cilindro_base(float radio, float altura)
{
	return cilindro_circunferencia(radio,altura)*radio;
}
float cilindro_volumen(float radio, float altura)
{
	return cilindro_base(radio,altura)*altura;
}
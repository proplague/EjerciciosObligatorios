#include "busqueda.h"

int busqueda(const int v[],size_t n, int objetivo, size_t izq,size_t der)
{
	if(der<=n&&izq>=0)
	{
		for(;izq!=der;izq++)
		{
			if(v[izq]==objetivo)
			{
				return izq;
			}
		}
	}
	return -1;
}

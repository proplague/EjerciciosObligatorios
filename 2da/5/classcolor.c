#include "classcolor.h"

void classcolor(double temp, espectral_t *clase)
{
	/*Eliji hacerlo por este metodo y no usando un vector y un for, porque tenes que usar mas espacio para almacenar los
	valores de los vectores y termina ocupando mas el programa.. no va a ir mas rapido*/
	if(RG1MIN<=temp&&temp<=RG1MAX) {*clase=O; return;}
	if(RG2MIN<=temp&&temp<RG2MAX) {*clase=B; return;}
	if(RG3MIN<=temp&&temp<RG3MAX) {*clase=A; return;}
	if(RG4MIN<=temp&&temp<RG4MAX) {*clase=F; return;}
	if(RG5MIN<=temp&&temp<RG5MAX) {*clase=G; return;}
	if(RG6MIN<=temp&&temp<RG6MAX) {*clase=K; return;}
	if(RG7MIN<=temp&&temp<RG7MAX) {*clase=M; return;}
	return;
}
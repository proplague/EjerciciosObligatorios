#define MSJ_INGRESO "Ingrese la temperatura del cuerpo celeste"
#define MSJ_CLASS "El color del cuerpo celeste"
#define MSJ_ERROR "Fuera de rango de clasificacion"
#define MAXCLASS 7
#define CLASS1 'O'
#define CLASS2 'B'
#define CLASS3 'A'
#define CLASS4 'F'
#define CLASS5 'G'
#define CLASS6 'K'
#define CLASS7 'M'
/* CLASS 1 RANGE */
#define RG1MIN 28000
#define RG1MAX 50000
/* CLASS 2 RANGE */
#define RG2MIN 9600
#define RG2MAX 28000
/* CLASS 3 RANGE */
#define RG3MIN 7100
#define RG3MAX 9600
/* CLASS 4 RANGE */
#define RG4MIN 5700
#define RG4MAX 7100
/* CLASS 5 RANGE */
#define RG5MIN 4600
#define RG5MAX 5700
/* CLASS 6 RANGE */
#define RG6MIN 3200
#define RG6MAX 4600
/* CLASS 7 RANGE */
#define RG7MIN 1700
#define RG7MAX 3200

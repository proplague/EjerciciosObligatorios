#ifndef CLASSCOLOR__H 
#define CLASSCOLOR__H 1
#include <stdio.h>
#include <stdlib.h>
#include "define5.h"
#endif

typedef enum {O=CLASS1,B=CLASS2,A=CLASS3,F=CLASS4,G=CLASS5,K=CLASS6,M=CLASS7,NOT}espectral_t;

void classcolor(double temp, espectral_t *clase);

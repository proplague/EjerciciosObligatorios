#include "productomatriz.h"

void producto_matriz(int col1,int fil1, int col2, int fil2)
{
	int i,j,k;
	int matrix1[fil1-1][col1-1];
	int matrix2[fil2-1][col2-1];
	int matrix3[fil1-1][col2-1];
	
	printf("%s %s\n",MSJ_INGRESE_A,TAGA);
	for(i=0;i<fil1;i++){
        for(j=0;j<col1;j++)
		{
			printf("%s%d%d >> ",TAGA,i+1,j+1); 
			scanf("%d",&k);
			limpiar_buff();
			matrix1[i][j] = k; /*suponemos que no pone chars y floats*/
		}
	}
	printf("%s %s\n",MSJ_INGRESE_A,TAGB);
	for(i=0;i<fil2;i++){
        for(j=0;j<col2;j++)
		{
			printf("%s%d%d >> ",TAGB,i+1,j+1);
			scanf("%d",&k);
			limpiar_buff();
			matrix2[i][j] = k; /*suponemos que no pone chars y floats*/
		}
	}
	for (i=0;i<fil2-1;i++)
    {
		for (j=0;j<col2;j++)
        { 
		  matrix3[i][j]=0;
          for (k=0;k<col1;k++)
          {
			matrix3[i][j]= matrix3[i][j]+ matrix1[i][k]*matrix2[k][j];
          }
       }
    }
	printf("%s\n",MSJ_MATRIZPROD);
	for(i=0;i<fil1;i++)
	{
        for(j=0;j<col2;j++)
		{
            printf("%3d ",matrix3[i][j]);
		}
        printf("\n");
	  }
}
void ingreso_datos(int *col, int *fil)
{
	printf("%s\n",MSJ_INGRESE_COL);
	scanf("%d",col);
	limpiar_buff();
	printf("%s\n",MSJ_INGRESE_FIL);
	scanf("%d",fil);
	limpiar_buff();
}
void limpiar_buff(void)
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF); 
}
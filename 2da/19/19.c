#include "19.h"

int main(void)

{
	int i,sumando,producto,col,fil;
	int matrix[MAX_FIL][MAX_COL];
	
	ingreso_datos(&col,&fil);
	if(col<=0||fil<=0||col>MAX_COL||fil>MAX_FIL)
	{
		return EXIT_FAILURE;
	}
	cargarMatriz(matrix,col,fil);
	imprimirMatriz(matrix,col,fil);
	printf("%d)%s\n%d)%s\n%d)%s\n%d)%s\n%d)%s\n",MENU1,MSJ_TZ,MENU2,MSJ_MULT,MENU3,MSJ_SUM,MENU4,MSJ_TRANS,MENU5,MSJ_SIG);
	printf("%d)%s\n%d)%s\n%d)%s\n",MENU6,MSJ_DET,MENU7,MSJ_MAYOR,MENU8,MSJ_MAY_COL);
	scanf("%d",&i);
	limpiar_buff();
	switch(i)
	{
		case OPC1: 	traza(matrix,col,fil);
					break;
		case OPC2: 	printf("%s\n",MSJ_INGRESE_NUM);
					scanf("%d",&producto);
					limpiar_buff();
					product_por_escalar(matrix,col,fil,producto);
					imprimirMatriz(matrix,col,fil);
					break;
		case OPC3:	printf("%s\n",MSJ_INGRESE_NUM);
					scanf("%d",&sumando);
					limpiar_buff();
					sumar_acomponentes(matrix,col,fil,sumando);
					imprimirMatriz(matrix,col,fil);
					break;
		case OPC4:	imprimirMatrizTrans(matrix,col,fil);
					break;
		case OPC5:	signomatriz(matrix,col,fil);
					break;
		case OPC6:	det_matriz(matrix,col,fil);
					break;
		case OPC7:	mayorvalor(matrix,col,fil);
					break;
		case OPC8:	mayorvalor_suma(matrix,col,fil);
					break;
		default: return EXIT_FAILURE;
	}
return EXIT_SUCCESS;
}
	

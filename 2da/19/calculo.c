#include "19.h"

void mayorvalor_suma(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int i,j,max,suma=0;
	
	for(j=0,i=0;j<fil;j++)
		{
			if(matriz[j][i]<0)
			{
			suma+=matriz[j][i]*(-1);
            }
			suma+=matriz[j][i];
		}
	max=suma;
	for(i=1,suma=0;i<col;i++,suma=0){
        for(j=0;j<fil;j++)
		{
			suma+=matriz[j][i];
        }
		if(suma>max)
			{
				max=suma;
			}
    }
	printf(">> %d",max);
}
void mayorvalor(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int i,j,max;
	
	max=matriz[0][0];
	for(i=0;i<fil;i++){
        for(j=0;j<col;j++)
		{
            if(matriz[i][j]>max)
			{
				max=matriz[i][j];
			}
		}
    }
	printf(">> %d",max);
}
void cargarMatriz(int matriz[MAX_FIL][MAX_COL],int col,int fil){
    int i,j,k;
	printf("%s %s\n",MSJ_INGRESE_A,TAGA);
    for(i=0;i<fil;i++){
        for(j=0;j<col;j++)
		{
			printf("%s%d%d >> ",TAGA,i+1,j+1); 
			scanf("%d",&k);
			limpiar_buff();
            matriz[i][j] = k; /*suponemos que no pone chars y floats*/
		}
    }
}
void imprimirMatriz(int matriz[MAX_FIL][MAX_COL],int col,int fil){
    int i,j;
    for(i=0;i<fil;i++)
	{
        for(j=0;j<col;j++)
        printf("%3d ",matriz[i][j]);
        printf("\n");
    }
}
void limpiar_buff(void)
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF); 
}
void traza(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int i,j=0;
	
	if(col==fil)
	{
	for(i=0;i<fil;i++)
	{
		j+=matriz[i][i];
	}
	printf("%d\n",j);
	}
	return;
}
void product_por_escalar(int matriz[MAX_FIL][MAX_COL],int col,int fil,int producto)
{
	int i,j;
	for(i=0;i<fil;i++){
        for(j=0;j<col;j++)
		{
            matriz[i][j] *= producto; 
		}
    }	
}

void sumar_acomponentes(int matriz[MAX_FIL][MAX_COL],int col,int fil,int sumando)
{
	int i,j;
	for(i=0;i<fil;i++){
        for(j=0;j<col;j++)
		{
            matriz[i][j] += sumando; 
		}
    }
}
void imprimirMatrizTrans(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int i,j;
    for(i=0;i<col;i++){
        for(j=0;j<fil;j++)
		{printf("%3d ",matriz[j][i]);}
        printf("\n");
        }
}
void signomatriz(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int i,j,signo,total=0;
    for(i=0;i<fil;i++)
	{
        for(j=0,signo=0;j<col;j++)
		{
			if(matriz[i][j]>=0)
			{
				signo++;
			}
		}
		if(signo!=col)
		{
			printf("%s\n",MSJ_MAT_NEG);
			return;
		}
        printf("\n");
		total+=j;
    }
	printf("%s\n",MSJ_MAT_POS);
}

void det_matriz(int matriz[MAX_FIL][MAX_COL],int col,int fil)
{
	int det;
	
	if((fil==2)&&(col==2))
	{
		det=((matriz[0][0]*matriz[1][1])-(matriz[1][0]*matriz[0][1])); /*algoritmo*/
		printf(">> %d\n",det);
	}
	if((fil==3)&&(col==3))
	{
		det=(matriz[0][0]*((matriz[1][1]*matriz[2][2])-(matriz[2][1]*matriz[1][2])));
		det-=(matriz[1][0]*((matriz[0][1]*matriz[2][2])-(matriz[2][1]*matriz[0][2])));
		det+=(matriz[2][0]*((matriz[0][1]*matriz[1][2])-(matriz[1][1]*matriz[0][2])));
		printf(">> %d\n",det);
	}
	return;
}
void ingreso_datos(int *col, int *fil)
{
	printf("%s\n",MSJ_INGRESE_COL);
	scanf("%d",col);
	limpiar_buff();
	printf("%s\n",MSJ_INGRESE_FIL);
	scanf("%d",fil);
	limpiar_buff();
}

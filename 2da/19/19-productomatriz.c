#include "productomatriz.h"

int main(void)
{
	int col1,col2,fil1,fil2;
	printf("%s %s\n",MSJ_INGRESE_A,TAGA);
	ingreso_datos(&col1,&fil1);
	printf("%s %s\n",MSJ_INGRESE_A,TAGB);
	ingreso_datos(&col2,&fil2);
	if(col1<=0||fil1<=0||col2<=0||fil2<=0)
	{
		return EXIT_FAILURE;
	}
	producto_matriz(col1,fil1,col2,fil2);
	return EXIT_SUCCESS;
}


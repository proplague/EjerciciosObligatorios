#ifndef MATRIZ__H 
#define MATRIZ__H 1
#include <stdio.h>
#include <stdlib.h>
#include "define19.h"
#endif

void limpiar_buff(void);
void cargarMatriz(int matriz[MAX_FIL][MAX_COL],int col, int fil);
void traza(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void product_por_escalar(int matriz[MAX_FIL][MAX_COL],int col,int fil,int producto);
void sumar_acomponentes(int matriz[MAX_FIL][MAX_COL],int col,int fil,int sumando);
void imprimirMatriz(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void imprimirMatrizTrans(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void signomatriz(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void det_matriz(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void ingreso_datos(int *col, int *fil);
void mayorvalor(int matriz[MAX_FIL][MAX_COL],int col,int fil);
void mayorvalor_suma(int matriz[MAX_FIL][MAX_COL],int col,int fil);
#define MAX_FIL 10
#define MAX_COL 10
#define MENU1 1
#define TAGA "A"
#define TAGB "B"
#define MENU2 2
#define MENU3 3
#define MENU4 4
#define MENU5 5
#define MENU6 6
#define MENU7 7
#define MENU8 8
#define OPC1 1
#define OPC2 2
#define OPC3 3
#define OPC4 4
#define OPC5 5
#define OPC6 6
#define OPC7 7
#define OPC8 8
#define MSJ_MAYOR "Mayor numero de la matriz"
#define MSJ_MAY_COL "La mayor suma de la columna"
#define MSJ_TZ "Traza"
#define MSJ_DET "Determinante, Solo 2x2 y 3x3"
#define MSJ_SUM "Sumar componente componente"
#define MSJ_MULT "Multiplicar numero a matriz"
#define MSJ_INGRESE_NUM "Ingrese numero"
#define MSJ_TRANS "Imprime Transpuesta"
#define MSJ_MAT_POS "Matriz es positiva"
#define MSJ_MAT_NEG "Matriz es negativa"
#define MSJ_SIG "Imprime signo matriz"
#define MSJ_INGRESE_COL "Ingrese Nro Columnas"
#define MSJ_INGRESE_FIL "Ingrese Nro Filas"
#define MSJ_INGRESE_A "Ingrese matriz"
#define MSJ_MATRIZPROD "La matriz resultante"

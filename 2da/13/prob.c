#include "prob.h"

int func_a(){
	double u;
	int f;
	
	srand(time(NULL));
	u = ((double)rand() / (double)RAND_MAX) ;
    printf("%s:",MSJ_RAND);
	printf("%lf\n", u);
	if(u <= 0.2 && u >= 0){
		f = 0;
		printf("%d\n", f);
	}
	else{
		f = 1;
		printf("%d\n", f);
	}
	return f;
}
int func_b(){
	double u;
	int f;
	
	srand(time(NULL));
	u = ((double)rand() / RAND_MAX) ;
	printf("%s:",MSJ_RAND);
	printf("%lf\n", u);
	if(u>=0 && u<=0.1){
		f = 3;
		printf("%d\n", f);
	}
	else if(u>0.1 && u<= 0.65){
		f = 5;
		printf("%d\n", f);
	}
	else{
		f = 9;
		printf("%d\n", f);
	}

	return f;
}
void moneda(){
	double u;
	
	srand(time(NULL));
	u = ((double)rand() / RAND_MAX) ;
	printf("%s:",MSJ_RAND);
	printf("%lf\n", u);
	if(u >=0 && u < 0.5){
		printf("%s\n",MSJ_CARA);
	}
	else{
		printf("%s\n",MSJ_SECA);
	}
}
int dado(){
	double u;
	
	srand(time(NULL));
	u = ((double)rand() / RAND_MAX) ;
	printf("%s:",MSJ_RAND);
	printf("%lf\n", u);
	if(u >= 0 && u <= (double)(1/6)){
		printf("|     |\n|  *  |\n|     |\n");
	}
	if(u > (double)1/6 && u <= (double)2/6){
		printf("|    *|\n|     |\n|*    |\n");
	}
	if(u > (double)2/6 && u <= (double)3/6){
		printf("|    *|\n|  *  |\n|*    |\n");
	}
	if(u > (double)3/6 && u <= (double)4/6){
		printf("|*   *|\n|     |\n|*   *|\n");
	}
	if(u > (double)4/6 && u <= (double)5/6){
		printf("|*   *|\n|  *  |\n|*   *|\n");
	}
	if(u > (double)5/6 && u <= 1.0){
		printf("|*   *|\n|*   *|\n|*   *|\n");
	}
	return EXIT_SUCCESS;
}
#include "prob.h"

int main (void){
	int id;

	printf("%s\n",MSJ_INGRESO);
	printf("%d)%s 'A'\n",OPC_1,MSJ_FUNC_A);
	printf("%d)%s 'B'\n",OPC_2,MSJ_FUNC_B);
	printf("%d)%s '%s-%s'\n",OPC_3,MSJ_FUNC_CC,MSJ_CARA,MSJ_SECA);
	printf("%d)%s\n",OPC_4,MSJ_SIM_DADO);
	scanf("%d", &id);
	switch(id)
	{
		case 1:
			func_a();				
			break;
		case 2:
			func_b();
			break;
		case 3:
			moneda();
			break;
		case 4:
			dado();
			break;
		default:
			fprintf(stderr, "%s", MSJ_ERROR_ING);
			break;
	}
	return EXIT_SUCCESS;
}
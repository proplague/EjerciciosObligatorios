#include "tiempo.h"

int main(void)
{
	int sec,hs,min;
	
	if(ingreso_datos(&sec)==ST_NEG)
	{ return EXIT_FAILURE; }
	calculo_tiempo(&sec,&hs,&min);
	printf("%s >> \n%s:%s:%s\n",MSJ_CONV,MSJ_HS,MSJ_MIN,MSJ_SEG);
	printf("%d:%d:%d",hs,min,sec); 
	return EXIT_SUCCESS;
}
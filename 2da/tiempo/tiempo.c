#include "tiempo.h"

status_t ingreso_datos(int *s)
{
	printf("%s\n",MSJ_INGRESE_TIEMPO);
	scanf("%d",s);
	limpiar_buff();
	if(*s<0)
	{
		return ST_NEG;
	}
	return ST_OK;
}
void limpiar_buff(void)
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF); 
}
status_t calculo_tiempo(int *s, int *hs, int *min)
{
	int aux;
	
	if((*s%60)==0) 
	{  
		*min=*s/60;
		*hs=*min/60; 
		*s=0;
		return ST_OK;
	}
	aux= *s/60;
	*s-=(aux*60);
	*min=aux;
	aux= *min/60;
	*min-=(aux*60);
	*hs=aux;
	return ST_OK;
}
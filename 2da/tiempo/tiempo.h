#ifndef TIME__H 
#define TIME__H 1
#include <stdio.h>
#include <stdlib.h>
#include "define.h"
#endif

typedef enum{ST_NEG,ST_OK}status_t;

void limpiar_buff(void);
status_t calculo_tiempo(int *s, int *hs, int *min);
status_t ingreso_datos(int *s);
#include <stdio.h>
#include <stdlib.h>
#include "defines43.h"

float rendimiento(float *kmtotal, float *cargatotal, float *costotal);

int main () {
	int i;
	float promedio,aux,mayor,menor;
	float kmtotal=0, cargatotal=0, costotal=0;
	for(i=0,promedio=0,aux=0,mayor=0;(aux=rendimiento(&kmtotal,&cargatotal,&costotal))>0;i++)
	{
		if((aux<menor)||(promedio==0)){menor=aux;}
		promedio+=aux;
		if(aux>mayor){mayor=aux;}
		
	}
	if(aux<=EXIT)
	{
		return 0;
	}
	
	printf("%s: %.2f[%s]\n%s >> %.2f[%s]\n%s >> %.2f[%s]\n%s >> %.2f[%s]\n%s >> %.2f[%s]\n%s >> %.2f[%s]\n",MSJ_PROMEDIO,(promedio/i),MSJ_RENDUNIDAD,MSJ_MEJOR_REND,mayor,MSJ_RENDUNIDAD,MSJ_PEOR_REND,menor,MSJ_RENDUNIDAD,MSJ_DIST,kmtotal,MSJ_LONG,MSJ_CONSUMO,cargatotal,MSJ_COMB,MSJ_COSTO,costotal,MSJ_PESOS);
	
	return EXIT_SUCCESS;
}

float rendimiento(float *kmtotal, float *cargatotal, float *costotal)
{
	char c;
	float kmrec, preciolt, montofinal, promedio;
	
	printf("%s ",MSJ_INGRESE);
	printf("%s: \n",MSJ_KM);
	scanf("%f",&kmrec);
	while ((c=getchar()) != '\n' && c != EOF); 
	if(kmrec<0) { return 0;}
	printf("%s ",MSJ_INGRESE);
	printf("%s: \n",MSJ_PRECIO);
	scanf("%f",&preciolt);
	while ((c=getchar()) != '\n' && c != EOF); 
	if(preciolt<0) { return EXIT;}
	printf("%s ",MSJ_INGRESE);
	printf("%s: \n",MSJ_MONTO);
	scanf("%f",&montofinal);
	while ((c=getchar()) != '\n' && c != EOF); 
	if(montofinal<0) { return EXIT;}
	promedio = ((100*(*cargatotal+=(montofinal/preciolt)))/kmrec);
	*kmtotal+=kmrec;
	*costotal+=montofinal;
	return promedio;
}
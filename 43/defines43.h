#define MSJ_INGRESE "Ingrese"
#define MSJ_KM "los kilometros recorridos"
#define MSJ_PRECIO "el precio del litro de combustible"
#define MSJ_MONTO "el monto total utilizado en esta compra"
#define MSJ_PROMEDIO "Rendimiento de este tanque"
#define MSJ_RENDUNIDAD "L/100km"
#define MSJ_LONG "Km"
#define MSJ_COMB "L"
#define MSJ_PESOS "$"

#define MSJ_MEJOR_REND "El mejor rendimiento"
#define MSJ_PEOR_REND "El peor rendimiento"
#define MSJ_DIST "La distancia total recorrida"
#define MSJ_CONSUMO "Combustible total consumido"
#define MSJ_COSTO "El costo total del vehiculo"
/* EXIT tiene que ser valor negativo*/
#define EXIT -2
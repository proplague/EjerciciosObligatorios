#include <stdio.h>
#include <stdlib.h>
#include "definiciones.h"

void imprimir_dias(int dia);

typedef enum {DIA_0, DIA_1, DIA_2, DIA_3, DIA_4, DIA_5, DIA_6}dias_t;

int main(void)
{
	int dia; char c;
	if(PRIMERDIA <0 || PRIMERDIA >6) { return EXIT_FAILURE; }
	printf("%s\n",MSJ_BIENVENIDA);
	printf("%s: ",MSJ_NUMERO);
	scanf("%d",&dia);
	while ((c=getchar()) != '\n' && c != EOF); 
	if(dia>366||dia<=0) {return EXIT_FAILURE;}
	dia = (((dia-1)%7)+ PRIMERDIA)%7;
	imprimir_dias(dia);
	return EXIT_SUCCESS;
}

void imprimir_dias(int dia)
{
	dias_t day;
	printf("%s: ",MSJ_ANUNCIO);
	switch(day=dia)
	{case DIA_0: printf("%s",MSJ_DIA_0); break;
	case DIA_1: printf("%s",MSJ_DIA_1); break;
	case DIA_2: printf("%s",MSJ_DIA_2); break;
	case DIA_3: printf("%s",MSJ_DIA_3); break;
	case DIA_4: printf("%s",MSJ_DIA_4); break;
	case DIA_5: printf("%s",MSJ_DIA_5); break;
	case DIA_6: printf("%s",MSJ_DIA_6); break;
	default: return;
	}
	
	
	
}